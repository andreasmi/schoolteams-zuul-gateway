package com.noroff.schoolteams.gateway.zuul.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

public class PreFilter extends ZuulFilter {

	@Override
	public String filterType() {
		return "pre";
	}

	@Override
	public int filterOrder() {
		return 0;
	}

	@Override
	public boolean shouldFilter() {
		return true;
	}

	@Override
	public Object run() {
		RequestContext ctx = RequestContext.getCurrentContext();
		HttpServletRequest request = ctx.getRequest();

		String header = request.getHeader("Authorization");
		if (header == null || header.isEmpty() || !header.startsWith("Bearer ")) {
			ctx.setResponseStatusCode(401);
			ctx.setSendZuulResponse(false);
		} else {

			try{
				String url = "https://schoolteams-service-auth.herokuapp.com/api/v1/verify";
				String service = request.getRequestURI().replaceFirst("^/+", "").split("/")[0];

				RestTemplate restTemplate = new RestTemplate();

				Map<String, String> requestBody = new HashMap();
				requestBody.put("tokenstring", header);
				requestBody.put("service", service);

				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.APPLICATION_JSON);

				HttpEntity<?> requestEntity = new HttpEntity<>(requestBody, headers);

				ResponseEntity<?> responseEntity = restTemplate.postForEntity(
						url,
						requestEntity,
						String.class);
				HttpStatus statusCode = responseEntity.getStatusCode();
				if(statusCode != HttpStatus.OK){
					ctx.setResponseStatusCode(401);
					ctx.setSendZuulResponse(false);
				}

			} catch (Exception e){
				ctx.setResponseStatusCode(401);
				ctx.setSendZuulResponse(false);
			}
		}


		System.out.println(
				"Request Method : " + request.getMethod() + " Request URL : " + request.getRequestURL().toString());

		return null;
	}

}